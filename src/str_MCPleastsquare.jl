function distzeronrmdescent∂ᴹF(pb::MCPLeastSquares, x, M::FixedSparsityManifold)
    vminnorm = nothing
    hasnormaldescent = false

    vminnorm = NSP.∇f(pb, x)
    for (i, isact) in enumerate(M.nz_coords)
        if isact
            ## Extended subdiff
            vminnorm[i] = max( abs(vminnorm[i]) - pb.λ, 0 )
        else
            ## Exact derivative
            vminnorm[i] += NSP.∇MCP(x[i], pb.β, pb.λ)
        end
    end


    hasnormaldescent = !isnothing(findfirst( t-> t > 0, vminnorm[M.nz_coords] ))
    # @show hasnormaldescent
    # @show findfirst( t-> t > 0, vminnorm[M.nz_coords] )
    # @assert false

    return vminnorm, vminnorm, hasnormaldescent
end
