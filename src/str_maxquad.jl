function listallmanifolds(pb::MaxQuadPb, x, cx)
    srtprm = sortperm(cx; rev = true)
    actind = first(srtprm)

    Mset = OrderedSet([MaxQuadManifold(pb, vcat(actind, otherinds)) for otherinds in powerset(srtprm[2:end])])
    return Mset
end

function hasnormaldescentdir(pb::MaxQuadPb, x, M)
    manifold_codim(M) == 0 && return false
    return distzeronrmdescent∂ᴹF(pb, x, M)[3]
end

function isNewtonRaphsonDescent(pb, state, x, M)
    manifold_codim(M) == 0 && return true
    state.di_struct = StructDerivativeInfo(M, x)
    @timeit_debug "oracle struct" oracles_structure!(state.di_struct, state.di_fo, pb, M, x)
    st = state.di_struct
    @show norm(state.di_struct.hx), M

    d = IterativeSolvers.lsmr(state.di_struct.Jacₕ, -state.di_struct.hx)

    @assert is_differentiable(pb, x)
    v = ∂F_elt(pb, x)

    return dot(v, d) < 0
end

function smoothstep!(d, pb::MaxQuadPb, x, cx)
    actind = argmax(cx)
    # BUG: this does not work with linear gᵢ...
    d .= -(NSP.∇²gᵢ(pb, x, actind) + 1e-10I) \ NSP.∇gᵢ(pb, x, actind)
    return
end

function display_pointmanifoldopt(pb::MaxQuadPb, x, M; printlev = 0)
    if manifold_codim(M) == 0
        println("-> Optimality report")
        println("M               : ", M)
        println("dist(∇F(x), 0) : ", norm(∂F_elt(pb, x)))
        return
    end
    di_fo = FirstOrderDerivativeInfo(pb, x)
    di_struct = StructDerivativeInfo(M, x)
    oracles_firstorder!(di_fo, pb, x)
    @timeit_debug "oracle struct" oracles_structure!(di_struct, di_fo, pb, M, x)

    polytopepoint, α, normaldescent = distzeronrmdescent∂ᴹF(pb, x, M)

    # Threshold things to 0.
    # @show value.(polytopepoint)
    println("-> Optimality report")
    println("M               : ", M)
    println("|h(x)|          : ", norm(di_struct.hx))
    println("dist(∂ᴹF(x), 0) : ", norm(polytopepoint))
    println("normal descent  : ", normaldescent)

    return
end

function distzeronrmdescent∂ᴹF(pb::MaxQuadPb, x, M)
    polytopepoint = nothing
    α = nothing
    hasnormaldescent = false

    actinds = M.active_fᵢ_indices

    # Project zero on polytope
    r = length(actinds)
    n = length(x)
    P = zeros(n, r)
    for (i, iact) in enumerate(actinds)
        P[:, i] .= NSP.∇gᵢ(pb, x, iact)
    end
    # α = QuadProgSimplex.nearest_point_polytope(P)
    @timeit_debug "qpsimplex" α = QuadProgSimplex.qpsimplex(P, zeros(r))
    polytopepoint = P * α

    # Existence of a normal descent direction if one coordinate is null.
    hasnormaldescent = findfirst( t -> t == 0, α) !== nothing
    # @show polytopepoint, α, hasnormaldescent

    # @timeit_debug "da - OSQP" begin
    # model = Model(optimizer_with_attributes(OSQP.Optimizer, "polish" => true))
    # set_silent(model)

    # α = @variable(model, α[actinds])
    # polytopepoint = sum( α[i] .* NSP.∇gᵢ(pb, x, i) for i in actinds)
    # @objective(model, Min, dot(polytopepoint, polytopepoint))
    # @constraint(model, sum(α) == 1)
    # @constraint(model, α .>= 0)
    # JuMP.optimize!(model)

    # # Threshold things to 0.
    # res = Vector(value.(α))

    # polytopepoint = value.(polytopepoint)
    # α = Vector(value.(α))
    # hasnormaldescent = findfirst( t -> t < 1e2 * eps(Float64), res) !== nothing
    # end
    # @show polytopepoint, α, hasnormaldescent

    # return value.(polytopepoint), value.(α), hasnormaldescent
    return polytopepoint, α, hasnormaldescent
end
