# get_legendname(obj::NSBFGS) = "nsBFGS"
# get_legendname(obj::GradientSampling) = "GradientSampling"
get_legendname(::GlobalCompoNewtonOpt) = "GlobalNewton"
get_legendname(::VUbundle) = raw"$\mathcal{VU}$-bundle"

function get_curveparams(::NSBFGS, objid, nobjs, COLORS, MARKERS)
    objid = 2
    return OrderedDict(
        "mark" => MARKERS[mod(objid, length(MARKERS)) + 1],
        "color" => COLORS[mod(objid, length(COLORS)) + 1],
    )
end
function get_curveparams(::GradientSampling, objid, nobjs, COLORS, MARKERS)
    objid = 1
    return OrderedDict(
        "mark" => MARKERS[mod(objid, length(MARKERS)) + 1],
        "color" => COLORS[mod(objid, length(COLORS)) + 1],
    )
end
function get_curveparams(::GlobalCompoNewtonOpt, objid, nobjs, COLORS, MARKERS)
    objid = 3
    return OrderedDict(
        "mark" => MARKERS[mod(objid, length(MARKERS)) + 1],
        "color" => COLORS[mod(objid, length(COLORS)) + 1],
    )
end
function get_curveparams(::VUbundle, objid, nobjs, COLORS, MARKERS)
    objid = 4
    return OrderedDict(
        "mark" => MARKERS[mod(objid, length(MARKERS)) + 1],
        "color" => COLORS[mod(objid, length(COLORS)) + 1],
    )
end




function buildfigures(optimdata, optimizers, xfinals, pb, xopt, Mopt, Fopt, pbname::String; NUMEXPS_OUTDIR, plot_maxit, plot_maxtime)
    @info "building figures for $pbname"

    buildfigures_subopt(optimdata, Fopt, pbname, plot_maxit, plot_maxtime, NUMEXPS_OUTDIR)

    # Collect data of structured optimizers
    optimdatastr = filter( p -> isa(first(p), GlobalCompoNewtonOpt) || isa(first(p), VUbundle), optimdata)
    length(optimdatastr) > 0 && buildfigures_mandim(optimdatastr, Mopt, pbname, NUMEXPS_OUTDIR)

    # Build table
    @info "building table for $pbname"
    build_table(optimizers, xfinals, Mopt, pb, pbname; NUMEXPS_OUTDIR)
    return true
end


function buildfigures_mandim(optimdatastr, Mopt, pbname, NUMEXPS_OUTDIR)
    getabsc_it(optimizer, trace) = [state.it for state in trace]
    getord_dim(::GlobalCompoNewtonOpt, trace) = [manifold_dim(state.additionalinfo.M) for state in trace]
    getord_dim(::VUbundle, trace) = vcat(Mopt.pb.n, [state.additionalinfo.nₖ for state in trace[2:end]])
    fig = plot_curves(
        optimdatastr,
        getabsc_it,
        getord_dim;
        xlabel="iteration",
        ylabel=L"\dim(\M_k)",
        ymode = "normal",
        nmarks=1000,
        includelegend=false,
        horizontallines = isnothing(Mopt) ? [] : [manifold_dim(Mopt)]
    )
    savefig(fig, joinpath(NUMEXPS_OUTDIR, pbname * "_it_dim"))
    return
end

function buildfigures_subopt(optimdata, Fopt, pbname, plot_maxit, plot_maxtime, NUMEXPS_OUTDIR)
    getabsc_time(optimizer, trace) = [state.time for state in trace]
    getabsc_it(optimizer, trace) = [state.it for state in trace]
    getord_subopt(optimizer, trace) = [state.Fx - Fopt for state in trace]
    fig = plot_curves(
        optimdata,
        getabsc_time,
        getord_subopt;
        xlabel="time (s)",
        xmax = plot_maxtime,
        ylabel=L"F(x_k) - F^\star",
        nmarks=1000,
        includelegend=false,
    )
    savefig(fig, joinpath(NUMEXPS_OUTDIR, pbname * "_time_subopt"))

    fig = plot_curves(
        optimdata,
        getabsc_it,
        getord_subopt;
        xlabel="iteration",
        xmax = plot_maxit,
        ylabel=L"F(x_k) - F^\star",
        nmarks=1000,
        includelegend=false,
    )
    savefig(fig, joinpath(NUMEXPS_OUTDIR, pbname * "_it_subopt"))
    return
end


function build_table(optimizers, xfinals, Mopt, pb, pbname::String; NUMEXPS_OUTDIR)
    A = Any["algo" "|h(x)|" "distpartialext" "normalascentdirs"]

    for (o, x) in xfinals
        algname = optimizers[o]

        if manifold_codim(Mopt) == 0
            A = vcat(A, [algname "" norm(∂F_elt(pb, x)) ""])
        else
            di_fo = FirstOrderDerivativeInfo(pb, x)
            di_struct = StructDerivativeInfo(Mopt, x)
            oracles_firstorder!(di_fo, pb, x)
            oracles_structure!(di_struct, di_fo, pb, Mopt, x)
            polytopepoint, α, normaldescent = distzeronrmdescent∂ᴹF(pb, x, Mopt)

            A = vcat(A, [algname norm(di_struct.hx) norm(polytopepoint) !normaldescent])
        end
    end
    open(joinpath(NUMEXPS_OUTDIR, pbname * ".txt"), "w") do io
        writedlm(io, A, '&')
    end
end
