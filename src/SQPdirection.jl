function get_SQP_direction_direct!(
    dSQP, dₙ, pb, M, x::Vector{Tf}, structderivative; info=Dict()
) where {Tf}
    Z = nullspace(structderivative.Jacₕ)         # tangent space basis

    ## 1. Restoration step
    dₙ .= IterativeSolvers.lsmr(structderivative.Jacₕ, -structderivative.hx)

    ## 2. Reduced gradient and RHS
    g = Z' * structderivative.∇Fx
    v = -g - Z' * structderivative.∇²Lx * dₙ

    ## 3. Linear system solve
    u = (Z' * structderivative.∇²Lx * Z) \ v

    dSQP .= dₙ + Z * u
    return dSQP
end


function get_SQP_direction_CG!(
    dSQP, dₙ, pb, M, x::Vector{Tf}, structderivative; info=Dict()
) where {Tf}
    Z = nullspace(structderivative.Jacₕ)         # tangent space basis

    ## 1. Restoration step
    dₙ .= 0
    if norm(structderivative.hx) > 1e1 * eps(Tf)
        dₙ .= IterativeSolvers.lsmr(structderivative.Jacₕ, -structderivative.hx)
        # Jacₕ = structderivative.Jacₕ
        # hx = structderivative.hx
        # dₙ = - Jacₕ' * ((Jacₕ * Jacₕ') \ hx)
    end

    ## 2. Reduced gradient and RHS
    g = Z' * structderivative.∇Fx

    M = Z' * structderivative.∇²Lx * Z
    v = -g - Z' * structderivative.∇²Lx * dₙ

    ## 3. Linear system solve, precision du CG: cf p. 19 de
    # Chauvier, Fuduli, Gilbert (2003) A Truncated SQP Algorithm for Solving
    # Nonconvex Equality Constrained Optimization Problems, Springer US.
    # u = M \ v
    # norm_rgrad = norm(g)
    # ϵ_residual = min(0.5, sqrt(norm_rgrad)) * norm_rgrad    # Forcing sequence as sugested in NW, p. 168
    ϵ_residual = 1e-13
    u, res = solve_tCG(M, v; ν=1e-3, ϵ_residual, maxiter=1e5, printlev=0)
    # @show norm(u1 - u2)

    dSQP .= dₙ + Z * u
    return dSQP
end

function computeMaratoscorrection!(dMaratos, d::Vector{Tf}, pb, M, x, Jacₕ) where {Tf}
    hxd = zeros(Tf, size(Jacₕ, 1))
    if isa(pb, Eigmax)
        h!(hxd, M.eigmult, x .+ d, g(pb, x .+ d))
    else
        hxd = NSP.h(M, x .+ d)
    end
    IterativeSolvers.lsmr!(dMaratos, Jacₕ, -hxd; verbose = false)
    # dMaratos .= - Jacₕ' * ((Jacₕ * Jacₕ') \ hxd)
    # @show norm(NSP.h(M, x))
    # @show norm(NSP.h(M, x + d))
    # @show norm(NSP.h(M, x + d + dMaratos))
    return
end
