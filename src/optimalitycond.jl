"""
    $TYPEDSIGNATURES

Compute the norm and minimal norm subdiff element for `pb`, at point `x` relative to manifold `M`.
"""
function stoppingcrit(pb, M, x)
    hnorm = 0.0
    vmineltnorm = 0.0
    if manifold_codim(M) == 0
        hnorm = 0.
        vmineltnorm = norm(∂F_elt(pb, x))
    else
        vminnorm, _, hasnormaldescent = distzeronrmdescent∂ᴹF(pb, x, M)
        hnorm = norm(NSP.h(M, x))
        vmineltnorm = norm(vminnorm)
    end
    return hnorm, vmineltnorm
end
