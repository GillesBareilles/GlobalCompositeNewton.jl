module GlobalCompositeNewton

using IterativeSolvers

using LinearAlgebra
using Combinatorics     # for powerset
using Printf
using DataStructures
using Random
using EigenDerivatives
using NonSmoothProblems
using NonSmoothSolvers

import NonSmoothSolvers:
    initial_state,
    print_header,
    display_logs_header_post,
    display_logs_post,
    update_iterate!,
    get_minimizer_candidate,
    has_converged

using LocalCompositeNewton
import LocalCompositeNewton: prox_max, oracles_firstorder!, oracles_structure!, FirstOrderDerivativeInfo, StructDerivativeInfo, get_lambda, update_difirstorder!

using PlotsOptim
import PlotsOptim: get_legendname, get_curveparams
using LaTeXStrings
using DocStringExtensions

using TimerOutputs
using QuadProgSimplex
using QuadProgSpectraplex
using ConjugateGradient

using DelimitedFiles


# Setting numerical experiments default output directory
const NUMEXPS_OUTDIR_DEFAULT = joinpath(
    dirname(pathof(GlobalCompositeNewton)), "..", "numexps_output"
)
function __init__()
    if !isdir(NUMEXPS_OUTDIR_DEFAULT)
        mkdir(NUMEXPS_OUTDIR_DEFAULT)
    end
    @info "default output directory for numerical experiments is: " NUMEXPS_OUTDIR_DEFAULT
    return nothing
end
const GCN = GlobalCompositeNewton

include("identification.jl")
include("guessstructure.jl")
include("str_maxquad.jl")
include("str_eigmax.jl")

include("SQPdirection.jl")

include("optimalitycond.jl")

include("globalNewton.jl")
# include("globalquasinewton.jl")

# Float64 experiments
include("problems/maxquadBGLS.jl")
include("problems/eigmax.jl")
include("problems/F3d-Unu.jl")
include("problems/leastsquareMCP.jl")
include("str_MCPleastsquare.jl")

# BigFloat experiments
include("problems/maxquadBGLS_BigFloat.jl")
include("problems/eigmax_BigFloat.jl")

include("plots/makeplots.jl")

export optimize!
export GCN
export GlobalCompoNewtonOpt

end # module
