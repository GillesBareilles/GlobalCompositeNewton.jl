showvals(::MaxQuadPb, cx) = begin display(cx'); display(sortperm(cx; rev=true)') end
showvals(::Eigmax, cx) = nothing


function listproxmanifolds(pb::MaxQuadPb, cx)
    _, active_indices = prox_max(cx, 10)
    M = [ MaxQuadManifold(pb, active_indices[1:i]) for i in length(active_indices):-1:1 ]

    return M
end

function listproxmanifolds(pb::Eigmax, cx)
    _, active_indices = prox_max(eigvals(cx), 10)
    rmax = min(6, length(active_indices))

    M = [ EigmaxManifold(pb, r) for r in rmax:-1:1 ]
    return M
end

function identifprocedure(state, pb, x, cx)
    # 1. List manifolds from prox with step 10
    @timeit_debug "listmans" Mlist = listproxmanifolds(pb, cx)

    # 2. take largest manifold with no normal descent directions
    iM = 1
    M = Mlist[iM]
    while true
        @timeit_debug "testact" minsubgradient, α, hasnormaldescent = distzeronrmdescent∂ᴹF(pb, x, M)
        if !hasnormaldescent
            break
        end
        iM += 1
        M = Mlist[iM]

        if iM == length(Mlist)
            @assert manifold_codim(M) == 0
            break
        end
    end

    return M
end

function identifprocedure(state, pb::MCPLeastSquares, x, cx)
    ∇fx = NSP.∇f(pb, x)

    # First, proximal identification step
    # Second, keep only coordinates such that 0 ∈ ∂ᴹF_i(x_i)
    inds = abs.(x) .< 1 .&& abs.(∇fx) .< pb.λ

    return FixedSparsityManifold(pb, inds)
end
