# Global quasi Newton Method
struct GlobalCompoQuasiNewtonOpt{Tf} <: NSS.NonSmoothOptimizer{Tf}
end

Base.@kwdef mutable struct GlobalCompoNewtonState{Tf,Tm,Tgx} <: NSS.OptimizerState{Tf}
    x::Vector{Tf} # point
    it::Int64     # iteration
    M::Tm         # current manifold
    γ::Tf         # current step
    di_fo::FirstOrderDerivativeInfo{Tf, Tgx}
    di_fonext::FirstOrderDerivativeInfo{Tf, Tgx}
end

function initial_state(
    o::GlobalCompoQuasiNewtonOpt, xinit::Vector{Tf}, pb; γ=100.0
) where {Tf}
    Minit = NSP.point_manifold(pb, xinit)
    state = GlobalCompoNewtonState(;
        x=xinit,
        it=1,
        M=NSP.point_manifold(pb, xinit),
        γ=Tf(γ),
        di_fo=FirstOrderDerivativeInfo(pb, xinit),
        di_fonext=FirstOrderDerivativeInfo(pb, xinit),
    )
    oracles_firstorder!(state.di_fo, pb, xinit)
    return state
end

#
### Printing
#
print_header(::GlobalCompoQuasiNewtonOpt) = println("**** GlobalCompoQuasiNewtonOpt algorithm")
get_minimizer_candidate(state::GlobalCompoNewtonState) = state.x

display_logs_header_post(::GlobalCompoQuasiNewtonOpt) = print("|dSQP|      α    M")
function display_logs_post(os, ::GlobalCompoQuasiNewtonOpt)
    ai = os.additionalinfo
    @printf "%.3e   %.2e   %s" os.additionalinfo.normd ai.α os.additionalinfo.M
end

function update_iterate!(state::GlobalCompoNewtonState{Tf, Tm, Tgx}, ::GlobalCompoQuasiNewtonOpt{Tf}, pb) where {Tf, Tm, Tgx}
    x = state.x

    ## Oracles
    cx = NSP.g(pb, x)

    M = identifprocedure(state, pb, x, cx)

    ## SQP step computation
    d = zeros(Tf, size(x))
    α = Inf
    if manifold_codim(M) == 0
        # HACK: exact newton step here
        α = smoothQuasiNewtonStep!(x, pb, d, cx, state)
    else
        oracles_structure!(state.di_struct, state.di_fo, pb, M, x)
        α = nonsmoothQuasiNewtonStep!(x, d, pb, cx, state, M)
    end

    oracles_firstorder!(state.di_fonext, pb, state.x)
    update_difirstorder!(state)
    state.di_fonext = state.di_fo
    state.M = M

    iteration_status = NSS.iteration_completed

    hnorm = Inf
    vmineltnorm = Inf

    hnorm, vmineltnorm = stoppingcrit(pb, M, x)
    if hnorm < 1e3 * eps(Tf) && vmineltnorm < 1e3 * eps(Tf)
        iteration_status = NSS.problem_solved
    end

    return (; :normd => norm(d), :M => M, :α => α), iteration_status
end


function smoothQuasiNewtonStep!(x, pb, d, cx, state)
    # HACK: exact newton step here
    smoothstep!(d, pb, x, cx)
    α = linesearch!(state, pb, x, d)
    return α
end

function nonsmoothQuasiNewtonStep!(x, dtmp, pb, cx, state, M)
    # SQP quasi Newton step
    dSQP = xx

    slope = dot(dSQP, ∂F_elt(pb, x))
    @assert slope < 0
    C = 0.1
    m = 1e-4
    α = Inf

    FxdSQP = F(pb, x + dSQP)
    Fx = F(pb, x)
    if FxdSQP ≤ Fx + m * slope
        state.x .+= dSQP
        α = 1.0
        return 1.0
    else
        C = 1e-1
        # NOTE: the linesearch is costly when the current manifold does not make sense locally.
        # in that case, we obsere a large funcitonal ascent.
        if norm(dₙ) ≤ C * norm(dSQP - dₙ)
            dcorr = similar(dtmp)
            @timeit_debug "dcorr" computeMaratoscorrection!(dcorr, dSQP, pb, M, x, state.di_struct.Jacₕ)
            # @show F(pb, x)
            # @show F(pb, x + dSQP)
            # @show F(pb, x + dSQP + dcorr)

            if F(pb, x + dSQP + dcorr) ≤ FxdSQP
                α = arcsearch!(state, pb, x, dSQP, dcorr)
                # @warn "arcsearch" α
            else
                α = linesearch!(state, pb, x, dSQP)
                # @info "linesearch 1" α
            end
        else
            α = linesearch!(state, pb, x, dSQP)
            # @info "linesearch 2" α
        end
    end
    return α
end


function arcsearch!(state, pb, x, d, dcorr; m = 1e-4)
    @timeit_debug "slope" slope = dot(d, ∂F_elt(pb, x))
    @assert slope < 0

    @timeit_debug "backtracking" begin
    Fx = F(pb, x)
    β = 0.5
    slope = 0.0 # HACK
    α = 1.0
    xcand = x + α * d
    Fcand = F(pb, xcand)
    while !(Fcand ≤ Fx + α * m * slope)
        α *= β
        xcand .= x .+ α .* d .+ α^2 .* dcorr
        Fcand = F(pb, xcand)
    end
    end
    state.x .= xcand
    return α
end
function linesearch!(state, pb, x, d; m = 1e-4)
    return arcsearch!(state, pb, x, d, zeros(size(d)); m)
end
