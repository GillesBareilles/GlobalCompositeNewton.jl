# Global Newton Method
struct GlobalCompoNewtonOpt{Tf} <: NSS.NonSmoothOptimizer{Tf}
end

Base.@kwdef mutable struct GlobalCompoNewtonState{Tf,Tm,Tgx} <: NSS.OptimizerState{Tf}
    x::Vector{Tf} # point
    it::Int64     # iteration
    M::Tm         # current manifold
    γ::Tf         # current step
    di_fo::FirstOrderDerivativeInfo{Tf, Tgx}
    di_fonext::FirstOrderDerivativeInfo{Tf, Tgx}
    di_struct::StructDerivativeInfo{Tf}
end

function initial_state(
    o::GlobalCompoNewtonOpt, xinit::Vector{Tf}, pb; γ=100.0
) where {Tf}
    Minit = NSP.point_manifold(pb, xinit)
    state = GlobalCompoNewtonState(;
        x=xinit,
        it=1,
        M=NSP.point_manifold(pb, xinit),
        γ=Tf(γ),
        di_fo=FirstOrderDerivativeInfo(pb, xinit),
        di_fonext=FirstOrderDerivativeInfo(pb, xinit),
        di_struct=StructDerivativeInfo(Minit, xinit),
    )
    oracles_firstorder!(state.di_fo, pb, xinit)
    return state
end

#
### Printing
#
print_header(::GlobalCompoNewtonOpt) = println("**** GlobalCompoNewtonOpt algorithm")
get_minimizer_candidate(state::GlobalCompoNewtonState) = state.x

display_logs_header_post(::GlobalCompoNewtonOpt) = print("|dSQP|      α          M")
function display_logs_post(os, ::GlobalCompoNewtonOpt)
    ai = os.additionalinfo
    @printf "%.3e   %.2e   %s" os.additionalinfo.normd ai.α os.additionalinfo.M
end

function update_iterate!(state::GlobalCompoNewtonState{Tf, Tm, Tgx}, ::GlobalCompoNewtonOpt{Tf}, pb) where {Tf, Tm, Tgx}
    @timeit_debug "updateiterate" begin
    x = state.x

    ## Identification
    @timeit_debug "gx" cx = NSP.g(pb, x)

    @timeit_debug "identif" M = identifprocedure(state, pb, x, cx)

    if !areequal(M, state.M)
        @timeit_debug "struct update" state.di_struct = StructDerivativeInfo(M, x)
    end

    ## SQP step computation
    @timeit_debug "stepcomp" begin
    d = zeros(Tf, size(x))
    α = Inf
    if manifold_codim(M) == 0
        @timeit_debug "smooth ls" α = smoothNewtonStep!(x, pb, d, cx, state)
    else
        @timeit_debug "oracle struct" oracles_structure!(state.di_struct, state.di_fo, pb, M, x)
        @timeit_debug "nonsmooth ls" α = nonsmoothNewtonStep!(x, d, pb, cx, state, M)
    end
    end

    @timeit_debug "Updates" begin
        oracles_firstorder!(state.di_fonext, pb, state.x)
        Fxd = state.di_fonext.Fx
        update_difirstorder!(state)
        state.di_fonext = state.di_fo
        state.M = M
    end

    iteration_status = NSS.iteration_completed

    @timeit_debug "stopp crit" begin
        hnorm, vmineltnorm = stoppingcrit(pb, M, x)
        if hnorm < 1e3 * eps(Tf) && vmineltnorm < 1e3 * eps(Tf)
            iteration_status = NSS.problem_solved
        end
    end
    end
    return (; :normd => norm(d), :M => M, :α => α), iteration_status
end

function smoothNewtonStep!(x, pb, d, cx, state)
    smoothstep!(d, pb, x, cx)
    α = linesearch!(state, pb, x, d)
    return α
end

function nonsmoothNewtonStep!(x, dtmp, pb, cx, state, M)
    dₙ = similar(dtmp)
    # dSQP = similar(dtmp)
    dSQP = dtmp
    # @timeit_debug "SQP step" get_SQP_direction_direct!(dSQP, dₙ, pb, M, x, state.di_struct)
    @timeit_debug "SQP step" get_SQP_direction_CG!(dSQP, dₙ, pb, M, x, state.di_struct)
    @assert !isnan(dSQP[1])

    @timeit_debug "slope" slope = dot(dSQP, ∂F_elt(pb, x))

    # v = ∂F_elt(pb, x)
    # printstyled("⟨v, dₙ⟩: ", dot(v, dₙ), " \t⟨v, dₜ⟩: ", dot(v, dSQP - dₙ), "\n", color=:blue)
    C = 0.1
    m = 1e-4
    α = Inf

    @timeit_debug "global bt" begin
    FxdSQP = F(pb, x + dSQP)
    Fx = F(pb, x)
    if FxdSQP ≤ Fx + m * slope
        state.x .+= dSQP
        α = 1.0
    else
        C = 1e-1
        # NOTE: the linesearch is costly when the current manifold does not make sense locally.
        # in that case, we observe a large funcitonal ascent.
        if norm(dₙ) ≤ C * norm(dSQP - dₙ)
            dcorr = similar(dtmp)
            @timeit_debug "dcorr" computeMaratoscorrection!(dcorr, dSQP, pb, M, x, state.di_struct.Jacₕ)

            if F(pb, x + dSQP + dcorr) ≤ FxdSQP
                α = arcsearch!(state, pb, x, dSQP, dcorr)
                # @info "arcsearch" α
            else
                α = linesearch!(state, pb, x, dSQP)
                # @info "linesearch 1 (Maratos pt is worse than SQP pt, already not good)" α
            end
        else
            α = linesearch!(state, pb, x, dSQP)
            # @info "linesearch 2 (Maratos unlikely)" α
        end
    end
    end
    return α
end


function arcsearch!(state, pb, x, d, dcorr; m = 1e-4)
    @timeit_debug "slope" slope = dot(d, ∂F_elt(pb, x))
    # @assert slope < 0

    @timeit_debug "backtracking" begin
    Fx = F(pb, x)
    β = 0.5
    slope = 0.0 # HACK
    α = 1.0
    xcand = x + α * d
    Fcand = F(pb, xcand)
    while !(Fcand ≤ Fx + α * m * slope)
        α *= β
        xcand .= x .+ α .* d .+ α^2 .* dcorr
        Fcand = F(pb, xcand)
    end
    end
    state.x .= xcand
    return α
end
function linesearch!(state, pb, x, d; m = 1e-4)
    return arcsearch!(state, pb, x, d, zeros(size(d)); m)
end
