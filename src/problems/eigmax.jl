function expe_eigmax(NUMEXPS_OUTDIR=NUMEXPS_OUTDIR_DEFAULT)
    Tf = Float64

    n, m = 25, 50
    pb = get_eigmax_affine(; m, n, seed=1864)

    # Initial point is perturbed minimum
    xopt = [0.18843385778159064, 0.3177797820667674, 0.343401482066522, -0.2780574918178362, -0.13402424141824798, -0.12921801804005137, -0.5566694894469032, -0.6007416403508792, 0.05910339069844592, 0.1770591172089212, 0.08556362274891832, -0.0266661037406984, -0.23677406310435925, -0.4819948736367132, 0.06585159302682411, 0.04851511424815355, -0.3925108044977492, -0.2492759692456895, 0.5381272220038654, 0.2599738152088106, -0.5646166510000448, 0.15500520551179048, -0.26412122660871273, 0.3668471291585293, -0.20803834263886967]
    Fopt = prevfloat(F(pb, xopt))
    Mopt = EigmaxManifold(pb, 3)
    x = xopt + 1e-1 * ones(n)

    optparams_precomp = OptimizerParams(;
        iterations_limit=2, trace_length=0, time_limit=0.5
    )

    ## Define solvers to be run on problem
    optimdata = OrderedDict()
    xfinals = OrderedDict()
    time_limit = 3
    iterations_limit = 200
    plot_maxit = 100
    plot_maxtime = 2


    optimizers = OrderedDict(
        GradientSampling(x) => "Gradient Sampling",
        NSBFGS{Tf}(; ϵ_opt=1e-15) => "nsBFGS",
        VUbundle(Newton_accel = true) => "VUalgo",
        GlobalCompoNewtonOpt{Tf}() => "GlobalNewton",
    )
    getM(o, os, osext) = deepcopy(os.M)
    os_gcn = OrderedDict{Symbol,Function}(:M => getM)

    for o in keys(optimizers)
        optparams = OptimizerParams(; iterations_limit, trace_length= 100, time_limit)
        try
            _ = NSS.optimize!(pb, o, x; optparams=optparams_precomp)
            xfinal, tr = NSS.optimize!(pb, o, x; optparams, optimstate_extensions = isa(o, GlobalCompoNewtonOpt) ? os_gcn : OrderedDict{Symbol, Function}())
            optimdata[o] = tr
            xfinals[o] = xfinal
        catch e
            @error "error running solver" o e
        end
    end

    ## Build figures
    for x in values(xfinals)
        display_pointmanifoldopt(pb, x, Mopt)
    end

    return buildfigures(optimdata, optimizers, xfinals, pb, xopt, Mopt, Fopt, "glob-eigmax"; NUMEXPS_OUTDIR, plot_maxit, plot_maxtime)
end
