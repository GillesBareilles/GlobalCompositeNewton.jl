function expe_maxquad(NUMEXPS_OUTDIR=NUMEXPS_OUTDIR_DEFAULT)
    Tf = Float64
    pb = MaxQuadBGLS(Tf)
    xopt = [
        0.12625658077472543,
        0.03437830256204086,
        0.006857198326981548,
        -0.026360658246337897,
        -0.06729492268974148,
        0.2783995007519937,
        -0.07421866454469361,
        -0.1385240478372969,
        -0.08403122312533247,
        -0.03858030977273088,
    ]
    Fopt = -8.4140833459641495e-01 - 1e-14
    Mopt = MaxQuadManifold(pb, [2, 3, 4, 5])
    x = xopt .+ 1e1 .* ones(Tf, 10)

    optparams_precomp = OptimizerParams(;
        iterations_limit=2, trace_length=0, time_limit=0.5
    )

    ## Define solvers to be run on problem
    optimdata = OrderedDict()
    xfinals = OrderedDict()
    time_limit = 0.1
    iterations_limit = 30000
    plot_maxit = 300
    plot_maxtime = 0.03

    optimizers = OrderedDict(
        GradientSampling(x) => "Gradient Sampling",
        NSBFGS{Tf}(; ϵ_opt=1e-15) => "nsBFGS",
        VUbundle(Newton_accel = true) => "VUalgo",
        GlobalCompoNewtonOpt{Tf}() => "GlobalNewton",
    )
    getM(o, os, osext) = deepcopy(os.M)
    os_gcn = OrderedDict{Symbol,Function}(:M => getM)

    for o in keys(optimizers)
        optparams = OptimizerParams(; iterations_limit, trace_length= 10, time_limit)
        try
            _ = NSS.optimize!(pb, o, x; optparams=optparams_precomp)
            xfinal, tr = NSS.optimize!(pb, o, x; optparams, optimstate_extensions = isa(o, GlobalCompoNewtonOpt) ? os_gcn : OrderedDict{Symbol, Function}())
            optimdata[o] = tr
            xfinals[o] = xfinal
        catch e
            @error "error running solver" o e
        end
    end

    ## Build figures
    for (o, x) in xfinals
        println("++ ", get_legendname(o))
        display_pointmanifoldopt(pb, x, Mopt)
    end

    return buildfigures(optimdata, optimizers, xfinals, pb, xopt, Mopt, Fopt, "glob-maxquadBGLS"; NUMEXPS_OUTDIR, plot_maxit, plot_maxtime)
end
