function FirstOrderDerivativeInfo(pb::MCPLeastSquares, x::Vector{Tf}) where {Tf}
    n = length(x)
    gx = similar(x)
    Tgx = typeof(gx)
    return FirstOrderDerivativeInfo{Tf, Tgx}(
        zeros(Tf, n), Tf(0), similar(gx), zeros(Tf, size(gx, 1)), zeros(Tf, n, n)
    )
end


function oracles_firstorder!(di::FirstOrderDerivativeInfo{Tf}, pb::MCPLeastSquares, x) where {Tf}
    @info "calling MCP oracle"
    di.x .= x
    di.Fx = F(pb, x)
    di.gx .= g(pb, x)

    return
end

function areequal(M::FixedSparsityManifold{Tpb}, N::FixedSparsityManifold{Tpb}) where Tpb
    return M.nz_coords == N.nz_coords
end


function expe_leastsquareMCP(NUMEXPS_OUTDIR=NUMEXPS_OUTDIR_DEFAULT)
    Tf = Float64
    pb = NSP.get_skglminstance()
    n = pb.n
    x = rand(n)

    gradsamp = false
    nsbfgs = false
    globnewton = true
    vubundle = false


    ## Define solvers to be run on problem
    optimdata = OrderedDict()
    time_limit = 5
    iterations_limit = 10000
    optparams_precomp = OptimizerParams(; iterations_limit = 3, show_trace = false)
    optparams = OptimizerParams(; iterations_limit, trace_length= 10000, time_limit)


    # Gradient sampling
    if gradsamp
        o = GradientSampling(x)
        _ = NSS.optimize!(pb, o, x; optparams=optparams_precomp)
        xfinal_gs, tr = NSS.optimize!(pb, o, x; optparams)
        optimdata[o] = tr
    end

    # nsBFGS
    if nsbfgs
        o = NSBFGS{Tf}(; ϵ_opt=1e-15)
        _ = NSS.optimize!(pb, o, x; optparams=optparams_precomp)
        xfinal_nsbfgs, tr = NSS.optimize!(pb, o, x; optparams)
        optimdata[o] = tr
    end

    # Global method, SQP steps
    if globnewton
        getM(o, os, osext) = deepcopy(os.M)
        optimstate_extensions = OrderedDict{Symbol,Function}(:M => getM)

        o = GlobalCompoNewtonOpt{Tf}()
        _ = NSS.optimize!(pb, o, x; optparams=optparams_precomp)
        xfinal_globalNewton, tr = NSS.optimize!(pb, o, x; optparams, optimstate_extensions)
        optimdata[o] = tr
    end

    if vubundle
        # VUbundle
        o = VUbundle(Newton_accel = true)
        _ = NSS.optimize!(pb, o, x; optparams=optparams_precomp)
        xfinal_VUalgo, tr = NSS.optimize!(pb, o, x; optparams)
        optimdata[o] = tr
    end

    ## Build figures
    Fopt = 0.0
    xopt = zeros(n)
    Mopt = nothing

    return buildfigures(optimdata, tr, pb, xopt, Mopt, Fopt, "glob-MCP"; NUMEXPS_OUTDIR)
end
