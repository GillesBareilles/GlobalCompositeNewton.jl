function expe_F3d_U0(; ν = 0, NUMEXPS_OUTDIR=NUMEXPS_OUTDIR_DEFAULT)
    Tf = Float64

    pb, xopt, Fopt, Mopt = NSP.F3d_U(ν; Tf)
    x = xopt .+ Tf[100, 33, -100]

    optparams_precomp = OptimizerParams(;
        iterations_limit=2, trace_length=0, time_limit=0.5
    )

    ## Define solvers to be run on problem
    optimdata = OrderedDict()
    xfinals = OrderedDict()
    time_limit, iterations_limit, plot_maxit, plot_maxtime = Dict(
        0 => (0.1, 300, 100, 0.002),
        1 => (0.1, 300, 100, 0.002),
        2 => (0.1, 300, 100, 0.002),
        3 => (0.1, 300, 100, 0.002),
    )[ν]

    optimizers = OrderedDict(
        GradientSampling(x) => "Gradient Sampling",
        NSBFGS{Tf}(; ϵ_opt=1e-15) => "nsBFGS",
        VUbundle(Newton_accel = true) => "VUalgo",
        GlobalCompoNewtonOpt{Tf}() => "GlobalNewton",
    )
    getM(o, os, osext) = deepcopy(os.M)
    os_gcn = OrderedDict{Symbol,Function}(:M => getM)

    for o in keys(optimizers)
        optparams = OptimizerParams(; iterations_limit, trace_length= 100, time_limit)
        try
            _ = NSS.optimize!(pb, o, x; optparams=optparams_precomp)
            xfinal, tr = NSS.optimize!(pb, o, x; optparams, optimstate_extensions = isa(o, GlobalCompoNewtonOpt) ? os_gcn : OrderedDict{Symbol, Function}())
            optimdata[o] = tr
            xfinals[o] = xfinal
        catch e
            @error "error running solver" o ν e
        end
    end

    ## Build figures
    for x in values(xfinals)
        display_pointmanifoldopt(pb, x, Mopt)
    end

    return buildfigures(optimdata, optimizers, xfinals, pb, xopt, Mopt, Fopt, "glob-F3d-U$(ν)"; NUMEXPS_OUTDIR, plot_maxit, plot_maxtime)
end
