function listallmanifolds(pb::Eigmax, x, cx)
    rmax = 1
    while manifold_codim(EigmaxManifold(pb, rmax + 1)) ≤ length(x)
        rmax += 1
    end
    Mset = OrderedSet([EigmaxManifold{Tf}(pb, r) for r in 1:rmax])
    return Mset
end


function hasnormaldescentdir(pb::Eigmax, x, M)
    manifold_codim(M) == 0 && return false
    return distzeronrmdescent∂ᴹF(pb, x, M)[3]
end

function smoothstep!(d, pb::Eigmax, x::Vector{Tf}, cx) where Tf
    eigmult = EigenDerivatives.EigMult(1, 1, x, pb.A)
    λmult = Tf[0]

    n = length(x)
    L = zeros(Tf, n, n)
    ∇²L!(L, eigmult, pb.A, x, λmult, cx)

    v = ∂F_elt(pb, x)
    d .= -L \ v
    return
end

function display_pointmanifoldopt(pb::Eigmax, x, M)
    di_fo = FirstOrderDerivativeInfo(pb, x)
    di_struct = StructDerivativeInfo(M, x)
    oracles_firstorder!(di_fo, pb, x)
    @timeit_debug "oracle struct" oracles_structure!(di_struct, di_fo, pb, M, x)
    update_refpoint!(M.eigmult, pb.A, x)

    # NOTE: subproblem solve
    Vshadow, _, normaldescent = distzeronrmdescent∂ᴹF(pb, x, M)

    # Threshold things to 0.
    # @show value.(polytopepoint)
    println("-> Optimality report")
    println("M               : ", M)
    println("|h(x)|          : ", norm(di_struct.hx))
    println("dist(∂ᴹF(x), 0) : ", norm(Vshadow))
    println("normal descent  : ", normaldescent)

    return
end


function distzeronrmdescent∂ᴹF(pb::Eigmax, x, M)
    r = M.eigmult.r
    # E = @view M.eigmult.Ē[:, end-r+1:end]
    # E = @view M.eigmult.Ē[:, end:-1:end-r+1] # BUG: these don't give correct information
    @timeit_debug "decomp" E = eigvecs(g(pb, x))[:, end-r+1:end]

    # @time specshadowpoint_, Z_, hasnormaldescent_ = solve_COSMO(E, pb, r, x)
    @timeit_debug "CHPrun" specshadowpoint, Z, hasnormaldescent = solve_CHP(E, pb, r, x)
    return specshadowpoint, Z, hasnormaldescent
end

function solve_CHP(Eᵣ, pb, r, x::Vector{Tf}) where Tf

    As = [ Symmetric(Eᵣ' * Aᵢ * Eᵣ) for Aᵢ in pb.A.As ]

    Z, Mspectraplex = QPSp.qpspectraplex(QPSp.SpectraplexShadow(As, r); show_trace = false)
    # Z, Mspectraplex = CHP.qpspectraplex(CHP.SpectraplexShadow(As, r))
    # @show Mspectraplex.r, Mspectraplex.k
    # @timeit_debug "CHPexec2" Z2, Mspectraplex2 = CHP.qpspectraplex(set)
    # showtrace = false
    # r, k = Mspectraplex.r, Mspectraplex.k
    # @timeit_debug "CHPexec" Z, Mspectraplex = CHP.optimize(set, zeros(Tf, r, r); showtrace, maxiter = 500)
    # if norm(Z - Z2) > 1e-10 || Mspectraplex != Mspectraplex2
    #     @warn norm(Z - Z2) Mspectraplex Mspectraplex2
    # end

    minnorm = EigenDerivatives.Dgconj(pb.A, x, Symmetric(Eᵣ * Z * Eᵣ'))
    # @show r, Mspectraplex

    return minnorm, Z, isspectraplexfaceinterior(Mspectraplex)
end

function isspectraplexfaceinterior(Mspectraplex::QPSp.SymmPosSemidefFixedRankUnitTrace)
    return Mspectraplex.k != Mspectraplex.r
end


# function solve_COSMO(E, pb, r, x)
#     QUIET = true
#     model = JuMP.Model(optimizer_with_attributes(COSMO.Optimizer,
#                                                  "accelerator" => with_options(AndersonAccelerator, mem = 15),
#                                                  "eps_abs" => 1e-10,
#                                                  "eps_rel" => 1e-10,
#                                                  "verbose" => !QUIET))

#     # Z in spectraplex
#     Z = @variable(model, [1:r, 1:r])
#     Zpsd = @constraint(model, Z in PSDCone())
#     sumone = @constraint(model, tr(Z) == 1.0)

#     # subdifferential of λ_max
#     V = E * Z * E'
#     Vshadow = EigenDerivatives.Dgconj(pb.A, x, V)

#     # SOCP objective
#     η = @variable(model)
#     @objective(model, Min, η)
#     @constraint(model, vcat(η, Vshadow) in SecondOrderCone())

#     JuMP.optimize!(model)
#     if termination_status(model) ∉ Set([MOI.OPTIMAL, MOI.LOCALLY_SOLVED])
#         @warn "Projection_zero_∂ᴹF: subproblem was not solved to optimality" termination_status(model) primal_status(model) dual_status(model)
#     end


#     res = eigvals(value.(Zpsd))
#     # @show eigvals(value.(Zpsd))
#     hasnormaldescent = findfirst( t -> t < 1e2 * eps(Float64), res) !== nothing

#     # Old code, not really necessary
#     spectreinri = sum((1-1e-10 .> eigvals(value.(Zpsd)) .> 1e-10)) == r
#     dualnull = norm(dual.(Zpsd)) < 1e-10
#     # @show dual.(Zpsd)
#     # @show dual.(sumone)
#     equivalence = (spectreinri && dualnull) || (!spectreinri && !dualnull)
#     if r > 1 && !equivalence
#         @warn "Activity of psd cone does not match eigvals of solution" norm(dual.(Zpsd)) eigvals(value.(Zpsd)) norm(dual.(sumone))
#         # @assert (termination_status(model) ∉ Set([MOI.OPTIMAL, MOI.LOCALLY_SOLVED]))
#     end

#     return value.(Vshadow), value.(Z), hasnormaldescent

# end
