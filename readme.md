# GlobalCompositeNewton.jl

<!-- [![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://GillesBareilles.github.io/GlobalCompositeNewton.jl/stable) -->
<!-- [![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://GillesBareilles.github.io/GlobalCompositeNewton.jl/dev) -->
[![Build Status](https://github.com/GillesBareilles/GlobalCompositeNewton.jl/actions/workflows/CI.yml/badge.svg?branch=master)](https://github.com/GillesBareilles/GlobalCompositeNewton.jl/actions/workflows/CI.yml?query=branch%3Amaster)
[![Coverage](https://codecov.io/gh/GillesBareilles/GlobalCompositeNewton.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/GillesBareilles/GlobalCompositeNewton.jl)
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)

*Setup* is as follow:
```julia
using Pkg
Pkg.update()
Pkg.Registry.add(RegistrySpec(url = "https://github.com/GillesBareilles/OptimRegistry.jl"))
Pkg.add(url = "https://github.com/GillesBareilles/GlobalCompositeNewton.jl", rev="master")
```

Experiments are executed with the commands:
```julia
using GlobalCompositeNewton

# Float64 experiments
GCN.expe_maxquad()
GCN.expe_eigmax()
GCN.expe_F3d_U0(ν = 0)
GCN.expe_F3d_U0(ν = 1)
GCN.expe_F3d_U0(ν = 2)
GCN.expe_F3d_U0(ν = 3)
```
